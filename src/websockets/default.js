import AWS from 'aws-sdk';
import createError from 'http-errors';

import middy from '@middy/core';
// import httpJsonBodyParser from '@middy/http-json-body-parser';
// import httpEventNormalizer from '@middy/http-event-normalizer';
// import httpErrorHandler from '@middy/http-error-handler';
// import cors from '@middy/http-cors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function deletePost(event, context) {

    console.log('event', event);

    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Methods': '*',
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({ message: 'default' }),
    };
}

export const handler = middy(deletePost);