import AWS from 'aws-sdk';
import createError from 'http-errors';

import middy from '@middy/core';
// import httpJsonBodyParser from '@middy/http-json-body-parser';
// import httpEventNormalizer from '@middy/http-event-normalizer';
// import httpErrorHandler from '@middy/http-error-handler';
// import cors from '@middy/http-cors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function deletePost(event, context) {

    console.log('event', event);
    const { connectionId: connectionID, domainName, stage } = event.requestContext;

    const data = {
        ID: connectionID,
        date: Date.now(),
        domainName,
        stage,
    };

    try {
        await dynamodb.put({
            TableName: 'ConnectionTable',
            Item: data,
        }).promise();
    } catch (error) {
        throw new createError.InternalServerError('Valio versh');
    }

    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Methods': '*',
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({ response: 'message' }),
    };
}

export const handler = middy(deletePost);