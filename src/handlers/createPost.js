import { nanoid } from 'nanoid'
import AWS from 'aws-sdk';
import createError from 'http-errors';

import middy from '@middy/core';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import httpEventNormalizer from '@middy/http-event-normalizer';
import httpErrorHandler from '@middy/http-error-handler';
import cors from '@middy/http-cors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function createPost(event, context) {
    const { title, description, imgRoute, category, user } = event.body;

    const post = {
        id: nanoid(),
        title,
        description,
        imgRoute,
        category,
        user,
        creationDate: Date.now(),
        comments: []
    };

    try {
        await dynamodb.put({
            TableName: 'PostsTable',
            Item: post,
        }).promise();
    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 201,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(post),
    };
}

export const handler = middy(createPost)
    .use(httpJsonBodyParser())
    .use(httpEventNormalizer())
    .use(httpErrorHandler())
    // .use(cors());