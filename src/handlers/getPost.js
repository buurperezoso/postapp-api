import AWS from 'aws-sdk';
import createError from 'http-errors';

import middy from '@middy/core';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import httpEventNormalizer from '@middy/http-event-normalizer';
import httpErrorHandler from '@middy/http-error-handler';
import cors from '@middy/http-cors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function getPost(event, context) {

    let post;
    const { id } = event.pathParameters;

    try {

        const result = await dynamodb.get({
            TableName: 'PostsTable',
            Key: { id }
        }).promise();

        post = result.Item;

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    if (!post) {
        throw new createError.NotFound('Post was not found');
    }

    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(post),
    };
}

export const handler = middy(getPost)
    .use(httpJsonBodyParser())
    .use(httpEventNormalizer())
    .use(httpErrorHandler())
    // .use(cors());