import AWS from 'aws-sdk';
import createError from 'http-errors';

import middy from '@middy/core';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import httpEventNormalizer from '@middy/http-event-normalizer';
import httpErrorHandler from '@middy/http-error-handler';
import cors from '@middy/http-cors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function getPosts(event, context) {

    let posts;

    try {

        const result = await dynamodb.scan({
            TableName: 'PostsTable'
        }).promise();

        posts = result.Items.sort((a, b) => {
            return new Date(b.creationDate) - new Date(a.creationDate);
        });

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(posts),
    };
}

export const handler = middy(getPosts)
    .use(httpJsonBodyParser())
    .use(httpEventNormalizer())
    .use(httpErrorHandler())
    // .use(cors());