import { nanoid } from 'nanoid'
import AWS from 'aws-sdk';
import createError from 'http-errors';

import middy from '@middy/core';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import httpEventNormalizer from '@middy/http-event-normalizer';
import httpErrorHandler from '@middy/http-error-handler';
import cors from '@middy/http-cors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function addComment(event, context) {

    const { id } = event.pathParameters;
    const { comment, user } = event.body;

    let newCommentsArray;
    let updatedPost;

    const newComment = {
        id: nanoid(),
        comment,
        user
    };

    try {

        const databasePost = await dynamodb.get({
            TableName: 'PostsTable',
            Key: { id }
        }).promise();

        const commentsArray = databasePost.Item.comments;
        newCommentsArray = [...commentsArray, newComment];

        const params = {
            TableName: 'PostsTable',
            Key: { id },
            UpdateExpression: 'set comments = :comments',
            ExpressionAttributeValues: {
                ':comments': newCommentsArray
            },
            ReturnValues: 'ALL_NEW',
        };

        const result = await dynamodb.update(params).promise();
        updatedPost = result.Attributes;

    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(updatedPost),
    };
}

export const handler = middy(addComment)
    .use(httpJsonBodyParser())
    .use(httpEventNormalizer())
    .use(httpErrorHandler())
    // .use(cors());