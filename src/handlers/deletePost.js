import { nanoid } from 'nanoid'
import AWS from 'aws-sdk';
import createError from 'http-errors';

import middy from '@middy/core';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import httpEventNormalizer from '@middy/http-event-normalizer';
import httpErrorHandler from '@middy/http-error-handler';
import cors from '@middy/http-cors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function deletePost(event, context) {

    const { id } = event.pathParameters;

    try {
        await dynamodb.delete({
            TableName: 'PostsTable',
            Key: { id },
        }).promise();
    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({ response: 'successful' }),
    };
}

export const handler = middy(deletePost)
    .use(httpJsonBodyParser())
    .use(httpEventNormalizer())
    .use(httpErrorHandler())
    // .use(cors());