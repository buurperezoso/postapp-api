import { nanoid } from 'nanoid'
import AWS from 'aws-sdk';
import createError from 'http-errors';

import middy from '@middy/core';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import httpEventNormalizer from '@middy/http-event-normalizer';
import httpErrorHandler from '@middy/http-error-handler';
import cors from '@middy/http-cors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function updatedPost(event, context) {

    const { id } = event.pathParameters;
    const { title, description, imgRoute, category, user } = event.body;

    const params = {
        TableName: 'PostsTable',
        Key: { id },
        UpdateExpression: 'set title = :title, description = :description, imgRoute = :imgRoute, category = :category',
        ExpressionAttributeValues: {
            ':title': title,
            ':description': description,
            ':imgRoute': imgRoute,
            ':category': category
        },
        ReturnValues: 'ALL_NEW',
    }

    let updatedPostObj;

    try {
        const result = await dynamodb.update(params).promise();
        updatedPostObj = result.Attributes;
    } catch (error) {
        throw new createError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(updatedPostObj),
    };
}

export const handler = middy(updatedPost)
    .use(httpJsonBodyParser())
    .use(httpEventNormalizer())
    .use(httpErrorHandler())
    // .use(cors());